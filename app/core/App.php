<?php
/**
 *
 */
class App{
  //propherty untuk menentukan controler methode dan parameter default
  protected $controller = 'Home';
  protected $method = 'index';
  protected $params = [];

  public function __construct()
  {

    $url = $this->parseURL();

      //ini untuk controller
    if (file_exists('../app/controllers/' . $url[0] . '.php')) { //mengecek apakah foldernya ada
       // jika ada buat instansiasinya
      $this->controller = $url[0]; //kelasnya kita intansiai
      unset($url[0]);
      //var_dump($url);
    }
    require_once '../app/controllers/' . $this->controller . '.php';
    $this->controller = new $this->controller;

    //ini untuk method
    if (isset($url[1]) ) {
        if (method_exists($this->controller, $url[1])) { //mengecek apakah methodenya ada
          $this->method = $url[1];
          unset($url[1]);
        }
    }
    //kelola Parameter
    if (!empty($url)) {
    $this->params =  array_values($url);
    //  var_dump($url);
    }
    //Jalankan controller dan method, serta kirimkan params jika ada/
    call_user_func_array([$this->controller, $this->method],$this->params);
  }

  public function parseURL(){
    if (isset($_GET['url'])) {
      $url = rtrim($_GET['url'], '/');// membersihkan url dari  slash
     $url = filter_var($url, FILTER_SANITIZE_URL); //membersihkan url dari
     //karakter2 aneh, yang memungkinkan url di hack
     $url = explode('/',$url); //url  kita pecah  dengan delimiternya stripslashes
     //slash2nya hilang, sting2nya berubah  menjadi element array.
      return $url;
      // code...
    }
  }
}



/*
*
*
*
*
*/

 ?>
