<?php
/**
 *
 */
class Mahasiswa extends Controller
{

  public  function index()
  {
   $data['judul'] = 'Daftar mahasiswa';
   $data['mhs'] = $this->model('Mahasiswa_model')->getMahasiswa(); //getMahasiswa adalah sebuah
   //method dari kelas mahasiswa model
   $this->view('templates/header', $data);
   $this->view('mahasiswa/index', $data);
   $this->view('templates/footer');
  }

  public  function detail($id)
  {
   $data['judul'] = 'Detail Mahasiswa';
   $data['mhs'] = $this->model('Mahasiswa_model')->getMahasiswabyId($id); //getMahasiswa adalah sebuah
   //method dari kelas mahasiswa model
   $this->view('templates/header', $data);
   $this->view('mahasiswa/detail', $data);
   $this->view('templates/footer');
  }
}


 ?>
